
(PT)
### Objetivo
Queremos testar suas habilidades para escrever cenários de testes e de programação.Que são necessário durante a automatização de testes.

Então o seu desafio vai ser o seguinte:

- Conceber cenários de 3 funcionalidades do instagram

- E resolver o seguinte problema:

Imagine que você está escrevendo um pedido de processamento de pedidos para uma grande empresa. No passado, esta empresa usava uma mistura bastante aleatória de práticas comerciais automáticas manuais e ad hoc para lidar com pedidos; eles agora querem colocar todas essas várias maneiras de receber ordens em um todo: sua aplicação. No entanto, eles (e seus clientes) chegaram a apreciar a diversidade de suas regras de negócios, e assim eles dizem que você terá que levar todas essas regras para o novo sistema.

Quando você entra em contato com as pessoas de entrada de pedidos existentes, você descobre que suas práticas comerciais se encaixam em caos: nenhuma linha de produtos possui o mesmo conjunto de regras de processamento. Para piorar, a maioria das regras não são gravadas: muitas vezes você diz algo como "oh, Carol no segundo andar lida com esse tipo de ordem".

Durante o primeiro dia de reuniões, você decidiu se concentrar em pagamentos e, em particular, no processamento exigido quando um pagamento foi recebido pela empresa. Você chega em casa, exausto, com um bloco legal cheio de trechos de regras, tais como:

Se o pagamento for para um produto físico, gere um recibo de embalagem para envio.

Se o pagamento for para um livro, crie um boleto de embalagem duplicado para o departamento de royalties.

Se o pagamento for para uma associação, ative essa associação.

Se o pagamento for uma atualização para uma associação, aplique a atualização.


E a cada dia, para o seu horror, você reúne mais e mais páginas dessas regras.

Agora você enfrenta a implementação deste sistema. As regras são complicadas e bastante arbitrárias. Além disso, você sabe que eles vão mudar: uma vez que o sistema fique vivo, todos os tipos de casos especiais sairão da madeira.


### Objetivos do Problema
Como você pode construir um sistema que seja flexível o suficiente para lidar com a complexidade e a necessidade de mudança? E como você pode fazer isso sem se condenar a anos e anos de apoio sem sentido?
Escreva um pouco de código para exemplificar a sua resposta.

### Deve conter

- Especificação de 3 funcionalidades do instagram (não importa se o app é Android, iOS ou versão Web) em Gherkin ou Gauge

- Cada especificação deve conter ao menos um cenário de teste

- Resposta do exercício 
### Submissão
O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:

1.Candidato fará um fork desse repositório (não irá clonar direto!)

2.Fará seu projeto nesse fork.

3.Commitará e subirá as alterações para o SEU fork.

4.Pela interface do Bitbucket, irá enviar um Pull Request.

5.Se possível deixar o fork público para facilitar a inspeção do código



### (EN)
### Goal
We want to test your skills in writing test and programming scenarios.These are required during test automation.
So your challenge will be as follows:
- Design scenarios of 3 instagram features
- And solve the following problem:
Imagine you’re writing an order processing application for a large company. In the past, this company used a fairly random mixture of manual and ad-hoc automated business practices to handle orders; they now want to put all these various ways of hanadling orders together into one whole: your application. However, they (and their customers) have come to cherish the diversity of their business rules, and so they tell you that you’ll have to bring all these rules forward into the new system.

When you go in to meet the existing order entry folks, you discover that their business practices border on chaotic: no two product lines have the same set of processing rules. To make it worse, most of the rules aren’t written down: you’re often told something like “oh, Carol on the second floor handles that kind of order.”

During first day of meetings, you’ve decided to focus on payments, and in particular on the processing required when a payment was received by the company. You come home, exhausted, with a legal pad full of rule snippets such as:

If the payment is for a physical product, generate a packing slip for shipping.

If the payment is for a book, create a duplicate packing slip for the royalty department.

If the payment is for a membership, activate that membership.

If the payment is an upgrade to a membership, apply the upgrade.


And each day, to your horror, you gather more and more pages of these rules.

Now you’re faced with implementing this system. The rules are complicated, and fairly arbitrary. What’s more, you know that they’re going to change: once the system goes live, all sorts of special cases will come out of the woodwork.

### Objectives

How can you tame these wild business rules? How can you build a system that will be flexible enough to handle both the complexity and the need for change? And how can you do it without condemming yourself to years and years of mindless support?
Write a little code to exemplify your answer.

### Must contain

- Specification of 3 instagram features (it does not matter if the app is Android, iOS or Web version) in Gherkin or Gauge

- Each specification must contain at least one test scenario

- Exercise response

### Submission
The candidate must implement the solution and send a pull request to this repository with the solution.

The Pull Request process works as follows:

1.Candidate will fork this repository (will not clone directly!)

2.It will make your project on that fork.

3.Commit and upload changes to your fork.

4.Through the Bitbucket interface, you will send a Pull Request.

5.If possible to leave the public fork to facilitate inspection of the code

